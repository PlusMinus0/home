function fish_prompt

    set -l retc red
	set -l last_status $status
    test $last_status = 0; and set retc blue

    set -q __fish_git_prompt_showupstream
    or set -g __fish_git_prompt_showupstream auto

    function _nim_prompt_wrapper
        set retc $argv[1]
        set -l field_name $argv[2]
        set -l field_value $argv[3]

        set_color normal
        set_color $retc
        echo -n '─'
        set_color -o green
        echo -n '['
        set_color normal
        test -n $field_name
        and echo -n $field_name:
        set_color $retc
        echo -n $field_value
        set_color -o green
        echo -n ']'
    end

    set_color $retc
    echo -n '# '

    if functions -q fish_is_root_user; and fish_is_root_user
        set_color -o red -b yellow
    else
        set_color green
    end

    echo -n $USER
	set_color normal
    set_color white
    echo -n @

    if test -z "$SSH_CLIENT"
        set_color blue
    else
        set_color -o cyan -b yellow
    end

    echo -n (prompt_hostname)
    set_color white
	echo -n ' in'
	set_color -o yellow
	echo -n ' '(prompt_pwd)

    # Vi-mode
    # The default mode prompt would be prefixed, which ruins our alignment.
    function fish_mode_prompt
    end

    if test "$fish_key_bindings" = fish_vi_key_bindings
        or test "$fish_key_bindings" = fish_hybrid_key_bindings
        set -l mode
        switch $fish_bind_mode
            case default
                set mode (set_color --bold red)N
            case insert
                set mode (set_color --bold green)I
            case replace_one
                set mode (set_color --bold green)R
                echo '[R]'
            case replace
                set mode (set_color --bold cyan)R
            case visual
                set mode (set_color --bold magenta)V
        end
        set mode $mode(set_color normal)
        _nim_prompt_wrapper $retc '' $mode
    end


    # Virtual Environment
    set -q VIRTUAL_ENV_DISABLE_PROMPT
    or set -g VIRTUAL_ENV_DISABLE_PROMPT true
    set -q VIRTUAL_ENV
    and _nim_prompt_wrapper $retc V (basename "$VIRTUAL_ENV")

    # git
	set_color normal
    set -l prompt_git (fish_git_prompt '%s')
    if test -n "$prompt_git"
		set_color white
		echo -n ' on '
		set_color -o cyan
		echo -n $prompt_git
	end

    # Date
	set_color normal
	set_color -d white
    echo -n ' '[(date +%X)]
    
	if test $last_status != 0
		set_color red
		echo -n ' 'E:$last_status
	end

    # New line
    echo

    # Background jobs
    set_color normal

    for job in (jobs)
        set_color $retc
        echo -n '# '
        set_color brown
        echo $job
    end

    set_color -o red
    echo -n '$ '
    set_color normal
end
