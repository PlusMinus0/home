set -g __fish_git_prompt_showuntrackedfiles true
set -g __fish_git_prompt_showupstream informative
set -g __fish_git_prompt_showdirtystate true
set -g __fish_git_prompt_showcolorhints true
set -g __fish_git_prompt_use_informative_chars true
set -g __fish_git_prompt_char_stateseparator ' ' 
#set -g __fish_git_prompt_show_informative_status true
