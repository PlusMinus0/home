#!/bin/env python3
from i3pystatus import Status
import socket

hostname_ = socket.gethostname()
color_ok_ = "#629755"
color_warn_ = "#CC7832"
color_err_ = "#D25252"

# To use fontawesome in this script, the ttf package has to be uninstalled
# See: https://bbs.archlinux.org/viewtopic.php?id=233098
# Also, font awesome icons don’t mix well with format options (:2.0f) so
# they they were put into text modules.

status = Status(logfile="/home/matthias/.log/i3pystatus.log")
log_level_ = 30

# Displays clock like this:
# Tue 30 Jul 11:59:46 PM KW31
status.register("clock",
                format="%a %-d %b %X KW%V ", )

if hostname_ == "medphys045":
	status.register('battery',
	                status={
		                'DPL': 'DPL',
		                'CHR': '🔌',
		                'DIS': '🔋',
		                'Full': '',
	                },
	                format="{status} {percentage_design:2.0f}% {remaining}",
	                no_text_full=True,
	                charging_color=None,
	                critical_color=color_err_,
	                interval=5,
	                )

# Shows your CPU temperature, if you have a Intel CPU
status.register("temp",
                format="{temp:.0f}°C", )

# Shows the average load of all cores and frequency
status.register("cpu_freq",
                format="@ {avgg} GHz",
                hints={"separator": False, "separator_block_width": 10})

status.register("cpu_usage", format="{usage:2.0f}%",
                hints={"separator": False, "separator_block_width": 10})

status.register("text", text="",
                hints={"separator": False, "separator_block_width": 5})

# Shows used mem / total mem
status.register("mem",
                divisor=1024 ** 3,
                color=None,
                warn_color="#CC7832",
                alert_color="#D25252",
                warn_percentage=50,
                format="{used_mem:2.1f}/{total_mem}G")

status.register("text", text="",
                hints={"separator": False, "separator_block_width": 5})

interfaces = {"PlusMinus": "enp3s0",
              "medphys421": "enp5s0"}
# Shows up and download total
if hostname_ == "PlusMinus" or hostname_ == "medphys421":
	status.register("network",
	                interface=interfaces[hostname_],
	                dynamic_color=False,
	                color_up=None,
	                color_down="#d25252",
	                format_up=" ↑{tx_tot_Mbytes:2.0f} ↓{rx_tot_Mbytes:2.0f}",
	                format_down="",
	                on_rightclick="",
	                on_downscroll="",
	                on_upscroll="", )
elif hostname_ == "medphys045":
	status.register("network",
	                interface="enp9s0u1u2",
	                dynamic_color=False,
	                color_up=None,
	                color_down="#d25252",
	                format_up=" ↑{tx_tot_Mbytes} ↓{rx_tot_Mbytes}",
	                format_down="",
	                on_rightclick="",
	                on_downscroll="",
	                on_upscroll="", )
	status.register("network", log_level=log_level_,
	                interface="wlp58s0",
	                dynamic_color=False,
	                color_up=None,
	                color_down="#d25252",
	                format_up="📶 {quality_bar}| {essid} ↑{tx_tot_Mbytes} ↓{rx_tot_Mbytes}",
	                format_down="📶",
	                on_leftclick="swaymsg exec nm-connection-editor",
	                on_doubleleftclick="ip link set up wlp58s0",
	                on_rightclick="",
	                on_downscroll="",
	                on_upscroll="",
	                hints={"separator": False, "separator_block_width": 10})

# Shows disk usage of /
# Format:
# 42/128G [86G]
if hostname_ in ["PlusMinus", "medphys421"]:
	status.register("disk",
	                path="/home",
	                format=" /home {percentage_used:2.1f}%", )
	if hostname_ == "PlusMinus":
		status.register("disk",
		                path="/steam",
		                format=" /steam {percentage_used:2.1f}%",
		                hints={"separator": False, "separator_block_width": 5})
	status.register("disk",
	                path="/",
	                format=" / {percentage_used:2.1f}%",
	                hints={"separator": False, "separator_block_width": 5})
elif hostname_ == "medphys045":
	status.register("disk",
	                path="/",
	                format=" / {percentage_used:2.1f}%")

# Shows pulseaudio volumes

if hostname_ in ["medphys045", "PlusMinus"]:
	status.register("pulseaudio", format_selected="🎵",
	                on_leftclick="change_sink",
	                color_muted="#d25252",
	                sink="alsa_output.pci-0000_00_1f.3.analog-stereo",
	                format="🔈{selected} {volume}", )

	if hostname_ == "PlusMinus":
		status.register("pulseaudio", format_selected="🎵", log_level=log_level_,
		                on_leftclick="change_sink",
		                color_muted="#d25252",
		                sink="alsa_output.usb-Logitech_Logitech_G930_Headset-00.analog-stereo",
		                format="🎧{selected} {volume}",
		                hints={"separator": False, "separator_block_width": 10})
elif hostname_ == "medphys421":
	status.register("pulseaudio", format_selected="🎵",
	                on_leftclick="change_sink",
	                color_muted="#d25252",
	                sink="alsa_output.pci-0000_0c_00.3.analog-stereo",
	                format="🔈{selected} {volume}", )

# Shows now playing
status.register("now_playing",
                format="{status} {artist} - {title}",
                status={
	                "pause": "⏸",
	                "play": "⏵",
	                "stop": "⏹",
                }, )

status.run()
