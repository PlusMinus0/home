vim.opt.autochdir = true

vim.opt.scrolloff = 50

-- Indentation
vim.opt.smartindent = true
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4

-- Search
vim.opt.incsearch = true

vim.wo.number = true
vim.wo.relativenumber = true
