return {
	'nvim-telescope/telescope.nvim',
	tag = '0.1.8',
	dependencies = { 'nvim-lua/plenary.nvim' },
	init = function () 
		local find_command =  { "fd", "--type", "f"} 

		local get_find_command = function() 
			local cwd = vim.fn.getcwd()
			if cwd == os.getenv('HOME') then
				find_command[#find_command+1] = "--no-ignore-vcs"
			end
			return find_command
		end
		require("telescope").setup {
			pickers = {
				find_files = {
					find_command = get_find_command()
				},
			}
		}
	end,
	config = function ()
		local builtin = require('telescope.builtin')
		vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
		vim.keymap.set('n', '<leader>fg', builtin.git_files, {})
		vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
		vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
	end
}
