export PATH=$HOME/.local/bin:$HOME/.emacs.d/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/usr/share/oh-my-zsh

if [ ! -f "${ZSH}/oh-my-zsh.sh" ]; then
	export ZSH=~/.oh-my-zsh
fi
source /etc/profile


# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="ys"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

source $ZSH/oh-my-zsh.sh

fpath=(~/.zsh/completions $fpath)

# History options
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=$HISTSIZE
setopt hist_ignore_all_dups

# Completions

# Use emacs keybinds
bindkey -e

#zstyle :compinstall filename '$HOME/.zshrc'

# Tab completion and autocorrection
#autoload -Uz compinit
#compinit

setopt correctall
setopt extendedglob

zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'


if command -v nvim &> /dev/null; then
	alias vim=nvim
fi

# Exports
export VISUAL="vim"

#export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock  # For rootless docker
export K3D_FIX_CGROUPV2=1  # Fix for k3d with k3s < 1.20.7

export QT_QPA_PLATFORM="wayland-egl"
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export DOCKER_BUILDKIT=1
#export GDK_BACKEND=wayland            # For GTK applications (firefox)
export MOZ_ENABLE_WAYLAND=1           # Firefox
export _JAVA_AWT_WM_NONREPARENTING=1  # For Java applacitions (JetBrains)
#export PS1="[%n@%M]%~ %% "
export XDG_CURRENT_DESKTOP=sway

export GTK_THEME=Adwaita:dark

export GPG_TTY=$(tty)

export LIBSEAT_BACKEND=logind

export VDPAU_DRIVER=radeonsi

function start_app() {
	$@ &; swaymsg move to scratchpad; wait $!; swaymsg scratchpad show; swaymsg floating toggle
}
compdef start_app=exec
alias start=start_app
#alias start='exec setsid'

# Aliases
alias sudosu='sudo -E -s'
alias sudo='nocorrect sudo'

if command -v lsd &> /dev/null; then
	alias ls=lsd
fi
alias ll='ls -lah'
alias sysupdate='yaourt -Syua --noconfirm' # --ignore=displaylink'
alias emacs='emacs -nw'
alias lmk='latexmk -f -pvc -pdflua -interaction=nonstopmode'
alias ssh='TERM=xterm ssh'

alias s='squeezelite -n wohnzimmer -f /var/log/squeezelite.log -s 192.168.1.60 -m 70:8b:cd:9e:fb:bc -a 80'

if [ "$(tty)" = "/dev/tty1" ]; then
	if [ -f /usr/bin/gnome-keyring-daemon ]; then
		eval $(/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)
		export SSH_AUTH_SOCK
	fi
	exec sway 1> sway.log 2> sway_err.log
fi


# AWS autocompletion
if command -v aws_completer &> /dev/null; then
	source $(dirname $(command -v aws_completer))/aws_zsh_completer.sh
fi

